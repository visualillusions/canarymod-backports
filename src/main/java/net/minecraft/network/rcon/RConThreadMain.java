package net.minecraft.network.rcon;


import net.canarymod.config.Configuration;
import net.canarymod.config.ServerConfiguration;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;


public class RConThreadMain extends RConThreadBase {

    private int h;
    private int i;
    private String j;
    private ServerSocket k;
    private String l;
    private Map m;
    private final ServerConfiguration config = Configuration.getServerConfig();
   
    public RConThreadMain(IServer iserver) {
        super(iserver, "RCON Listener");
        this.h = config.getRconPort();
        this.l = config.getRconPassword();
        this.j = iserver.y();
        this.i = iserver.z();
        if (0 == this.h) {
            this.h = this.i + 10;
            this.b("Invalid rcon port, using default port " + this.h); // CanaryMod: no writing config
        }

        if (0 == this.j.length()) {
            this.j = "0.0.0.0";
        }

        this.f();
        this.k = null;
    }

    private void f() {
        this.m = new HashMap();
    }

    private void g() {
        Iterator iterator = this.m.entrySet().iterator();

        while (iterator.hasNext()) {
            Entry entry = (Entry) iterator.next();

            if (!((RConThreadClient) entry.getValue()).c()) {
                iterator.remove();
            }
        }

    }

    public void run() {
        this.b("RCON running on " + this.j + ":" + this.h);

        try {
            while (this.a) {
                try {
                    Socket socket = this.k.accept();

                    socket.setSoTimeout(500);
                    RConThreadClient rconthreadclient = new RConThreadClient(this.b, socket);

                    rconthreadclient.a();
                    this.m.put(socket.getRemoteSocketAddress(), rconthreadclient);
                    this.g();
                } catch (SocketTimeoutException sockettimeoutexception) {
                    this.g();
                } catch (IOException ioexception) {
                    if (this.a) {
                        this.b("IO: " + ioexception.getMessage());
                    }
                }
            }
        } finally {
            this.b(this.k);
        }

    }

    public void a() {
        if (0 == this.l.length()) {
            this.c("No rcon password set in \'" + this.config.getFile().getFileName() + "\', rcon disabled!");
        } else if (0 < this.h && '\uffff' >= this.h) {
            if (!this.a) {
                try {
                    this.k = new ServerSocket(this.h, 0, InetAddress.getByName(this.j));
                    this.k.setSoTimeout(500);
                    super.a();
                } catch (IOException ioexception) {
                    this.c("Unable to initialise rcon on " + this.j + ":" + this.h + " : " + ioexception.getMessage());
                }

            }
        } else {
            this.c("Invalid rcon port " + this.h + " found in \'" + this.config.getFile().getFileName() + "\', rcon disabled!");
        }
    }
}
