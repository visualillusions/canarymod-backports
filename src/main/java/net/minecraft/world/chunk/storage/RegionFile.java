package net.minecraft.world.chunk.storage;


import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import net.minecraft.server.MinecraftServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class RegionFile {
    private static final Logger logger = LogManager.getLogger();

    private static final byte[] a = new byte[4096];
    private final File b;
    private RandomAccessFile c;
    private final int[] d = new int[1024];
    private final int[] e = new int[1024];
    private ArrayList f;
    private int g;
    private long h;
    private final ReadWriteLock fileLock = new ReentrantReadWriteLock();
   
    public RegionFile(File file1) {
        this.b = file1;
        this.g = 0;

        try {
            if (file1.exists()) {
                this.h = file1.lastModified();
            }

            this.c = new RandomAccessFile(file1, "rw");
            int i0;

            if (this.c.length() < 4096L) {
                for (i0 = 0; i0 < 1024; ++i0) {
                    this.c.writeInt(0);
                }

                for (i0 = 0; i0 < 1024; ++i0) {
                    this.c.writeInt(0);
                }

                this.g += 8192;
            }

            if ((this.c.length() & 4095L) != 0L) {
                for (i0 = 0; (long) i0 < (this.c.length() & 4095L); ++i0) {
                    this.c.write(0);
                }
            }

            i0 = (int) this.c.length() / 4096;
            this.f = new ArrayList(i0);

            int i1;

            for (i1 = 0; i1 < i0; ++i1) {
                this.f.add(Boolean.valueOf(true));
            }

            this.f.set(0, Boolean.valueOf(false));
            this.f.set(1, Boolean.valueOf(false));
            this.c.seek(0L);

            int i2;

            for (i1 = 0; i1 < 1024; ++i1) {
                i2 = this.c.readInt();
                this.d[i1] = i2;
                if (i2 != 0 && (i2 >> 8) + (i2 & 255) <= this.f.size()) {
                    for (int i3 = 0; i3 < (i2 & 255); ++i3) {
                        this.f.set((i2 >> 8) + i3, Boolean.valueOf(false));
                    }
                }
            }

            for (i1 = 0; i1 < 1024; ++i1) {
                i2 = this.c.readInt();
                this.e[i1] = i2;
            }
        } catch (IOException ioexception) {
            ioexception.printStackTrace();
        }

    }

    public DataInputStream a(int i0, int i1) {
        if (this.d(i0, i1)) {
            logger.error("REGION READ {}[{},{}] = out of bounds", b.getName(), i0, i1);
            return null;
        } else {
            try {
                fileLock.readLock().lock();
                int i2 = this.e(i0, i1);

                if (i2 == 0) {
                    return null;
                } else {
                    int i3 = i2 >> 8;
                    int i4 = i2 & 255;

                    if (i3 + i4 > this.f.size()) {
                        logger.error("REGION READ {}[{},{}] = invalid sector", b.getName(), i0, i1);
                        return null;
                    } else {
                        this.c.seek((long) (i3 * 4096));
                        int i5 = this.c.readInt();

                        if (i5 > 4096 * i4) {
                            logger.error("REGION READ {}[{},{}] = invalid length: {} > 4096 * {}", b.getName(), i0, i1, i5, i4);
                            return null;
                        } else if (i5 <= 0) {
                            logger.error("REGION READ {}[{},{}] = invalid length: {} <= 0", b.getName(), i0, i1, i5);
                            return null;
                        } else {
                            byte b0 = this.c.readByte();
                            byte[] abyte;

                            if (b0 == 1) {
                                abyte = new byte[i5 - 1];
                                this.c.read(abyte);
                                return new DataInputStream(new BufferedInputStream(new GZIPInputStream(new ByteArrayInputStream(abyte))));
                            } else if (b0 == 2) {
                                abyte = new byte[i5 - 1];
                                this.c.read(abyte);
                                return new DataInputStream(new BufferedInputStream(new InflaterInputStream(new ByteArrayInputStream(abyte))));
                            } else {
                                logger.error("REGION READ {}[{},{}] = unknown version {}", b.getName(), i0, i1, b0);
                                return null;
                            }
                        }
                    }
                }
            } catch (IOException ioexception) {
                logger.error(logger.getMessageFactory().newMessage("REGION READ {}[{},{}] = exception", b.getName(), i0, i1), ioexception);
                return null;
            } finally {
                fileLock.readLock().unlock();
            }
        }
    }

    public DataOutputStream b(int i0, int i1) {
        fileLock.writeLock().lock();
        return this.d(i0, i1) ? null : new DataOutputStream(new DeflaterOutputStream(new RegionFile.ChunkBuffer(i0, i1)));
    }

    protected void a(int i0, int i1, byte[] abyte, int i2) {
        try {
            int i3 = this.e(i0, i1);
            int i4 = i3 >> 8;
            int i5 = i3 & 255;
            int i6 = (i2 + 5) / 4096 + 1;

            if (i6 >= 256) {
                logger.error("REGION SAVE {}[{},{}] {}B = maximum chunk size exceeded: {} > 256", b.getName(), i0, i1, i2, i6);
                return;
            }

            if (i4 != 0 && i5 == i6) {
                logger.debug("REGION SAVE {}[{},{}] {}B = rewrite", b.getName(), i0, i1, i2);
                this.a(i4, abyte, i2);
            } else {
                int i7;

                for (i7 = 0; i7 < i5; ++i7) {
                    this.f.set(i4 + i7, Boolean.valueOf(true));
                }

                i7 = this.f.indexOf(Boolean.valueOf(true));
                int i8 = 0;
                int i9;

                if (i7 != -1) {
                    for (i9 = i7; i9 < this.f.size(); ++i9) {
                        if (i8 != 0) {
                            if (((Boolean) this.f.get(i9)).booleanValue()) {
                                ++i8;
                            } else {
                                i8 = 0;
                            }
                        } else if (((Boolean) this.f.get(i9)).booleanValue()) {
                            i7 = i9;
                            i8 = 1;
                        }

                        if (i8 >= i6) {
                            break;
                        }
                    }
                }

                if (i8 >= i6) {
                    logger.debug("REGION SAVE {}[{},{}] {}B = reuse", b.getName(), i0, i1, i2);
                    i4 = i7;
                    this.a(i0, i1, i7 << 8 | i6);

                    for (i9 = 0; i9 < i6; ++i9) {
                        this.f.set(i4 + i9, Boolean.valueOf(false));
                    }

                    this.a(i4, abyte, i2);
                } else {
                    logger.debug("REGION SAVE {}[{},{}] {}B = grow", b.getName(), i0, i1, i2);
                    this.c.seek(this.c.length());
                    i4 = this.f.size();

                    for (i9 = 0; i9 < i6; ++i9) {
                        this.c.write(a);
                        this.f.add(Boolean.valueOf(false));
                    }

                    this.g += 4096 * i6;
                    this.a(i4, abyte, i2);
                    this.a(i0, i1, i4 << 8 | i6);
                }
            }

            this.b(i0, i1, (int) (MinecraftServer.ar() / 1000L));
        } catch (IOException ioexception) {
            logger.error(logger.getMessageFactory().newMessage("REGION SAVE {}[{},{}] {}B = exception", b.getName(), i0, i1, i2), ioexception);
        }
    }

    private void a(int i0, byte[] abyte, int i1) throws IOException {
        this.c.seek((long) (i0 * 4096));
        this.c.writeInt(i1 + 1);
        this.c.writeByte(2);
        this.c.write(abyte, 0, i1);
    }

    private boolean d(int i0, int i1) {
        return i0 < 0 || i0 >= 32 || i1 < 0 || i1 >= 32;
    }

    private int e(int i0, int i1) {
        return this.d[i0 + i1 * 32];
    }

    public boolean c(int i0, int i1) {
        return this.e(i0, i1) != 0;
    }

    private void a(int i0, int i1, int i2) throws IOException {
        this.d[i0 + i1 * 32] = i2;
        this.c.seek((long) ((i0 + i1 * 32) * 4));
        this.c.writeInt(i2);
    }

    private void b(int i0, int i1, int i2) throws IOException {
        this.e[i0 + i1 * 32] = i2;
        this.c.seek((long) (4096 + (i0 + i1 * 32) * 4));
        this.c.writeInt(i2);
    }

    public void c() throws IOException {
        try {
            fileLock.writeLock().lock();
            if (this.c != null) {
                this.c.close();
            }
        } finally {
            fileLock.writeLock().unlock();
        }
    }

    class ChunkBuffer extends ByteArrayOutputStream {

        private int b;
        private int c;
      
        public ChunkBuffer(int p_i2000_2_, int p_i2000_3_) {
            super(8096);
            this.b = p_i2000_2_;
            this.c = p_i2000_3_;
        }

        public void close() throws IOException {
            RegionFile.this.a(this.b, this.c, this.buf, this.count);
            RegionFile.this.fileLock.writeLock().unlock();
        }
    }
}
