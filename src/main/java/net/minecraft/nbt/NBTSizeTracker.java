/*
 * Copyright (c) 2016 Visual Illusions Entertainment
 * All rights reserved.
 */

package net.minecraft.nbt;


public class NBTSizeTracker {

    public static final NBTSizeTracker a = new NBTSizeTracker(0L) {
        public void a(long l0) {
        }
    };

    private final long b;
    private long c;

    public NBTSizeTracker(long l0) {
        this.b = l0;
    }

    public void a(long p_a_1_) {
        this.c += p_a_1_ / 8L;
        if (this.c > this.b) {
            throw new RuntimeException("Tried to read NBT tag that was too big; tried to allocate: " + this.c + "bytes where max allowed: " + this.b);
        }
    }

    /*
     * UTF8 is not a simple encoding system, each character can be either
     * 1, 2, or 3 bytes. Depending on where it's numerical value falls.
     * We have to count up each character individually to see the true
     * length of the data.
     *
     * Basic concept is that it uses the MSB of each byte as a 'read more' signal.
     * So it has to shift each 7-bit segment.
     *
     * This will accurately count the correct byte length to encode this string, plus the 2 bytes for it's length prefix.
     */
    public static void readUTF(NBTSizeTracker tracker, String data) {
        tracker.a(16); //Header length
        if (data == null)
            return;

        int len = data.length();
        int utflen = 0;

        for (int i = 0; i < len; i++) {
            int c = data.charAt(i);
            if ((c >= 0x0001) && (c <= 0x007F)) utflen += 1;
            else if (c > 0x07FF) utflen += 3;
            else utflen += 2;
        }
        tracker.a(8 * utflen);
    }

}
