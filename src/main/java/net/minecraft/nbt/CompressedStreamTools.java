/*
 * Copyright (c) 2016 Visual Illusions Entertainment
 * All rights reserved.
 */

package net.minecraft.nbt;


import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.util.ReportedException;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


public class CompressedStreamTools {

    public static NBTTagCompound a(InputStream inputStream) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new GZIPInputStream(inputStream)));

        NBTTagCompound nbtTagCompound;

        try {
            nbtTagCompound = a((DataInput) dataInputStream, NBTSizeTracker.a);
        } finally {
            dataInputStream.close();
        }

        return nbtTagCompound;
    }

    public static void a(NBTTagCompound nbtTagCompound, OutputStream outputStream) throws IOException {
        DataOutputStream var2 = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(outputStream)));

        try {
            a(nbtTagCompound, (DataOutput) var2);
        } finally {
            var2.close();
        }

    }

    public static NBTTagCompound a(byte[] bytes0, NBTSizeTracker nbtSizeTracker) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new GZIPInputStream(new ByteArrayInputStream(bytes0))));

        NBTTagCompound nbtTagCompound;

        try {
            nbtTagCompound = a((DataInput) dataInputStream, nbtSizeTracker);
        } finally {
            dataInputStream.close();
        }

        return nbtTagCompound;
    }

    public static byte[] a(NBTTagCompound nbtTagCompound) throws IOException {
        ByteArrayOutputStream var1 = new ByteArrayOutputStream();
        DataOutputStream var2 = new DataOutputStream(new GZIPOutputStream(var1));

        try {
            a(nbtTagCompound, (DataOutput) var2);
        } finally {
            var2.close();
        }

        return var1.toByteArray();
    }

    public static NBTTagCompound a(DataInputStream dataInputStream) throws IOException {
        return a((DataInput) dataInputStream, NBTSizeTracker.a);
    }

    public static NBTTagCompound a(DataInput dataInput, NBTSizeTracker nbtSizeTracker) throws IOException {
        NBTBase var2 = a(dataInput, 0, nbtSizeTracker);

        if (var2 instanceof NBTTagCompound) {
            return (NBTTagCompound) var2;
        } else {
            throw new IOException("Root tag must be a named compound tag");
        }
    }

    public static void a(NBTTagCompound nbtTagCompound, DataOutput dataOutput) throws IOException {
        a((NBTBase) nbtTagCompound, dataOutput);
    }

    private static void a(NBTBase nbtBase, DataOutput dataOutput) throws IOException {
        dataOutput.writeByte(nbtBase.a());
        if (nbtBase.a() != 0) {
            dataOutput.writeUTF("");
            nbtBase.a(dataOutput);
        }
    }

    private static NBTBase a(DataInput dataInput, int i0, NBTSizeTracker nbtSizeTracker) throws IOException {
        byte var3 = dataInput.readByte();
        nbtSizeTracker.a(8); // Count everything!

        if (var3 == 0) {
            return new NBTTagEnd();
        } else {
            dataInput.readUTF(); //Count this string.
            nbtSizeTracker.a(32); //4 extra bytes for the object allocation.
            NBTBase var4 = NBTBase.a(var3);

            try {
                var4.a(dataInput, i0, nbtSizeTracker);
                return var4;
            } catch (IOException var8) {
                CrashReport var6 = CrashReport.a(var8, "Loading NBT data");
                CrashReportCategory var7 = var6.a("NBT Tag");

                var7.a("Tag name", (Object) "[UNNAMED TAG]");
                var7.a("Tag type", (Object) Byte.valueOf(var3));
                throw new ReportedException(var6);
            }
        }
    }
}
